require 'json'
require 'webrick'

module Phase4
  class Session
    # find the cookie for this app
    # deserialize the cookie into a hash
    def initialize(req)
      relavent_cookies = req.cookies.each.select{ |cookie| cookie.name == '_rails_lite_app' }
      if relavent_cookies[0] && relavent_cookies[0].value != nil
        @my_cookie_value = JSON.parse(relavent_cookies[0].value)
      else
        @my_cookie_value = {}
      end

    end

    def [](key)
      @my_cookie_value[key]
    end

    def []=(key, val)
      @my_cookie_value[key] = val
    end

    def store_session(res)
      name = '_rails_lite_app'
      value = @my_cookie_value.to_json
      cookie = WEBrick::Cookie.new(name, value)
      res.cookies << cookie
    end
  end
end
