require 'webrick'
require_relative '../lib/phase2/controller_base'

# http://www.ruby-doc.org/stdlib-2.0/libdoc/webrick/rdoc/WEBrick.html
# http://www.ruby-doc.org/stdlib-2.0/libdoc/webrick/rdoc/WEBrick/HTTPRequest.html
# http://www.ruby-doc.org/stdlib-2.0/libdoc/webrick/rdoc/WEBrick/HTTPResponse.html
# http://www.ruby-doc.org/stdlib-2.0/libdoc/webrick/rdoc/WEBrick/Cookie.html

class Phase2::ControllerBase
  attr_accessor :res
  attr_accessor :req

  def initialize(req,res)
    p "initialize"
    @req = req
    @res = res
  end

  def render_content(body, content_type)
    @res.body = body
    @res.content_type = content_type

    @already_built_response = true
  end

  def redirect_to(url)
    @res.status = 302
    @res["Location"] = url
  end
end







class MyController < Phase2::ControllerBase
  def go
    if @req.path == "/cats"
      render_content("hello cats! #{@res["Location"]}", "text/html")
    else
      redirect_to("/cats")
    end
  end
end

server = WEBrick::HTTPServer.new(Port: 3000)
server.mount_proc('/') do |req, res|
  MyController.new(req, res).go
end

trap('INT') { server.shutdown }
server.start
